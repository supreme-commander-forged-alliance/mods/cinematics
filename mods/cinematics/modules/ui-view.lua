


local callbackNearFov = function(value)
    local uiModel = import("/mods/cinematics/modules/ui-model.lua")
    uiModel.nearFov = value
    ConExecute("cam_NearFov " .. value) 
end

local callbackFarFov = function(value)
    local uiModel = import("/mods/cinematics/modules/ui-model.lua")
    uiModel.farFov = value
    ConExecute("cam_FarFov " .. value) 
end

local callbackNearPitch= function(value)
    local uiModel = import("/mods/cinematics/modules/ui-model.lua")
    uiModel.nearPitch = value
    ConExecute("cam_NearPitch " .. value) 
end

local callbackFarPitch= function(value)
    local uiModel = import("/mods/cinematics/modules/ui-model.lua")
    uiModel.farPitch = value
    ConExecute("cam_FarPitch " .. value) 
end

local callbackShadowLOD= function(value)
    local uiModel = import("/mods/cinematics/modules/ui-model.lua")
    uiModel.shadowLOD = value
    ConExecute("ren_ShadowLOD " .. value) 
end

local callbackShadowResolution= function(value)
    local uiModel = import("/mods/cinematics/modules/ui-model.lua")
    uiModel.shadowSize = value
    ConExecute("ren_ShadowSize " .. value) 
end

local callbackShadowCoeff = function(value)
    local uiModel = import("/mods/cinematics/modules/ui-model.lua")
    uiModel.shadowCoeff = value
    ConExecute("ren_ShadowCoeff " .. value) 
end

local percentage = 0.45

function CreateInterface(window, isReplay)

    local uiModel = import("/mods/cinematics/modules/ui-model.lua")
    
    window:Begin()

    window:BeginTabBar("main-tab-bar", { "General", "MoveTo", "Pan", "Spin" } )

        window:Divider()
        window:Space()

        if window:BeginTab("General") then 
            window:Text("A small mod that helps with creating cinematic shots. You ")
            window:Text("can navigate between various functionality using the tab bar.")
            window:Space()
        
            window:Text("Note that there are hotkeys defined under each tab. That is")
            window:Text("the only approach to interact with the mod. Note that the")
            window:Text("hotkeys always work even when you're in a different tab.")
            window:Space()

            window:Text("Unanimatable camera settings")
            window:Divider()

            local callbacks = { OnScrub = callbackNearFov }
            uiModel.nearFov = window:SliderFloatCB("Near Field of View#slider", 10, 180, uiModel.nearFov, callbacks)

            local callbacks = { OnScrub = callbackFarFov }
            uiModel.farFov = window:SliderFloatCB("Far Field of View#slider", 10, 180, uiModel.farFov, callbacks)

            local callbacks = { OnScrub = callbackNearPitch }
            uiModel.nearPitch = window:SliderFloatCB("Near Pitch#slider", 10, 180, uiModel.nearPitch, callbacks)

            local callbacks = { OnScrub = callbackFarPitch }
            uiModel.farPitch = window:SliderFloatCB("Far Pitch#slider", 10, 180, uiModel.farPitch, callbacks)

            local callbacks = { OnScrub = callbackShadowLOD }
            uiModel.shadowLOD = window:SliderFloatCB("Shadow LOD#slider", 100, 1000, uiModel.shadowLOD, callbacks)

            local callbacks = { OnScrub = callbackShadowResolution }
            uiModel.shadowResolution = window:SliderFloatCB("Shadow Resolution#slider", 100, 4096, uiModel.shadowResolution, callbacks)

            window:Space();

            window:Text("Hotkeys")
            window:Divider()

            for k, hotkey in uiModel.HotkeysGeneral do 
                window:TextWithLabel(hotkey.Keys, hotkey.Description, percentage)
            end
        end

        if window:BeginTab("MoveTo") then 

            window:Text("Settings")
            window:Divider()
            window:Space()
            window:TextWithLabel("Start at MoveTo Origin", uiModel.FormatBoolean(uiModel.MoveTo.UseOrigin), percentage)
            uiModel.MoveTo.Seconds = window:SliderFloat("Transition time#slider", 1, 20, uiModel.MoveTo.Seconds)

            window:Space()
    
            if uiModel.MoveTo.UseOrigin then 
                window:Text("MoveTo - Origin")
                window:Divider()
                window:TextWithLabel("Focus", uiModel.FormatVector(uiModel.MoveTo.Origin.Focus), percentage)
                window:TextWithLabel("Heading", uiModel.FormatFloat(uiModel.MoveTo.Origin.Heading), percentage)
                window:TextWithLabel("Pitch", uiModel.FormatFloat(uiModel.MoveTo.Origin.Pitch), percentage)
                window:TextWithLabel("Zoom", uiModel.FormatFloat(uiModel.MoveTo.Origin.Zoom), percentage)
                window:Space()
            end
    
            window:Text("MoveTo - Target")
            window:Divider()
            window:TextWithLabel("Focus", uiModel.FormatVector(uiModel.MoveTo.Target.Focus), percentage)
            window:TextWithLabel("Heading", uiModel.FormatFloat(uiModel.MoveTo.Target.Heading), percentage)
            window:TextWithLabel("Pitch", uiModel.FormatFloat(uiModel.MoveTo.Target.Pitch), percentage)
            window:TextWithLabel("Zoom", uiModel.FormatFloat(uiModel.MoveTo.Target.Zoom), percentage)
    
            window:Space();

            window:Text("Hotkeys")
            window:Divider()
    
            for k, hotkey in uiModel.HotkeysMoveTo do 
                window:TextWithLabel(hotkey.Keys, hotkey.Description, percentage)
            end
        end

        if window:BeginTab("Spin") then 

            window:TextWithLabel("Start at Spin Origin", uiModel.FormatBoolean(uiModel.Spin.UseOrigin), percentage)
            window:Space();

            if uiModel.Spin.UseOrigin then 
                window:Text("Spin - Origin")
                window:Divider()
                window:TextWithLabel("Focus", uiModel.FormatVector(uiModel.Spin.Origin.Focus), percentage)
                window:TextWithLabel("Heading", uiModel.FormatFloat(uiModel.Spin.Origin.Heading), percentage)
                window:TextWithLabel("Pitch", uiModel.FormatFloat(uiModel.Spin.Origin.Pitch), percentage)
                window:TextWithLabel("Zoom", uiModel.FormatFloat(uiModel.Spin.Origin.Zoom), percentage)
                window:Space()
            end


            window:Text("Spin - Speed")
            window:Divider()
            uiModel.Spin.HeadingRate, changed = window:SliderFloat("Heading rate#slider", -0.2, 0.2, uiModel.Spin.HeadingRate)
            uiModel.Spin.ZoomRate, changed = window:SliderFloat("Zoom rate#slider", -3, 3, uiModel.Spin.ZoomRate)

            window:Space();

            window:Text("Hotkeys")
            window:Divider()

            for k, hotkey in uiModel.HotkeysSpin do 
                window:TextWithLabel(hotkey.Keys, hotkey.Description, percentage)
            end
        end 

        if window:BeginTab("Pan") then 

            window:TextWithLabel("Start at Pan Origin", uiModel.FormatBoolean(uiModel.Pan.UseOrigin), percentage)
            window:Space();

            if uiModel.Pan.UseOrigin then 
                window:Text("Pan - Origin")
                window:Divider()
                window:TextWithLabel("Focus", uiModel.FormatVector(uiModel.Pan.Origin.Focus), percentage)
                window:TextWithLabel("Heading", uiModel.FormatFloat(uiModel.Pan.Origin.Heading), percentage)
                window:TextWithLabel("Pitch", uiModel.FormatFloat(uiModel.Pan.Origin.Pitch), percentage)
                window:TextWithLabel("Zoom", uiModel.FormatFloat(uiModel.Pan.Origin.Zoom), percentage)
                window:Space()
            end


            window:Text("Pan - Speed")
            window:Divider()
            uiModel.Pan.XRate, changed = window:SliderFloat("Pan X rate#slider", -2, 2, uiModel.Pan.XRate)
            uiModel.Pan.ZRate, changed = window:SliderFloat("Pan Z rate#slider", -2, 2, uiModel.Pan.ZRate)

            window:Space();

            window:Text("Hotkeys")
            window:Divider()

            for k, hotkey in uiModel.HotkeysPan do 
                window:TextWithLabel(hotkey.Keys, hotkey.Description, percentage)
            end

        end 


        -- todo: store orientation
        -- todo: jump to orientation
        -- todo: store direction
        -- todo: store speed

    window:EndTabBar("main-tab-bar")

    window:End()
end
