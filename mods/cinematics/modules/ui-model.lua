
--- Keeps track of whether things have been initialized or not

-- GENERIC SETTINGS

ConRenderUI = true

oNearFov = 80
oFarFov = 80
oNearPitch = 45
oFarPitch = 90

nearFov = oNearFov
farFov = oFarFov
nearPitch = oNearPitch
farPitch = oFarPitch

oShadowResolution = 1024
oShadowLOD = 500

shadowResolution = oShadowResolution
shadowLOD = oShadowLOD

oEaseInAndOut = true
easeInAndOut = oEaseInAndOut

-- HOTKEYS --

-- Keeps track of all the hotkey information
HotkeysGeneral = { }
HotkeysMoveTo = { }
HotkeysSpin = { }
HotkeysPan = { }

--- Formats the hotkey entry.
local function CreateHotkey (keys, description, func)
    return { Keys = keys, Description = description, Func = func }
end

table.insert(HotkeysGeneral, CreateHotkey("Ctrl-1", "Reset the camera",         'UI_Lua import("/mods/cinematics/modules/ui-controller.lua").ToggleReset()'))
table.insert(HotkeysGeneral, CreateHotkey("Ctrl-2", "Toggle UI",                'UI_Lua import("/mods/cinematics/modules/ui-controller.lua").ToggleUI()'))
table.insert(HotkeysGeneral, CreateHotkey("Ctrl-3", "Toggle ease in and out",   'UI_Lua import("/mods/cinematics/modules/ui-controller.lua").ToggleEaseInAndOut()'))
table.insert(HotkeysGeneral, CreateHotkey("Ctrl-4", "Toggle freecam",           'UI_Lua import("/mods/cinematics/modules/ui-controller.lua").ToggleFreeCam()'))
table.insert(HotkeysGeneral, CreateHotkey("Ctrl-5", "Toggle decal LOD",         'UI_Lua import("/mods/cinematics/modules/ui-controller.lua").ToggleIgnoreDecalLOD()'))
table.insert(HotkeysGeneral, CreateHotkey("Ctrl-6", "Toggle select boxes",      'UI_Lua import("/mods/cinematics/modules/ui-controller.lua").ToggleSelectBoxes()'))

table.insert(HotkeysMoveTo, CreateHotkey("Ctrl-Q", "Set 'MoveTo' origin",       'UI_Lua import("/mods/cinematics/modules/ui-controller.lua").SetMoveToOrigin()'))
table.insert(HotkeysMoveTo, CreateHotkey("Ctrl-W", "Set 'MoveTo' target",       'UI_Lua import("/mods/cinematics/modules/ui-controller.lua").SetMoveToTarget()'))
table.insert(HotkeysMoveTo, CreateHotkey("Ctrl-A", "Preview 'MoveTo' Origin",   'UI_Lua import("/mods/cinematics/modules/ui-controller.lua").PreviewMoveToOrigin()'))
table.insert(HotkeysMoveTo, CreateHotkey("Ctrl-S", "Preview 'MoveTo' Target",   'UI_Lua import("/mods/cinematics/modules/ui-controller.lua").PreviewMoveToTarget()'))
table.insert(HotkeysMoveTo, CreateHotkey("Ctrl-Z", "Preview 'MoveTo'",          'UI_Lua import("/mods/cinematics/modules/ui-controller.lua").PreviewMoveToMotion()'))
table.insert(HotkeysMoveTo, CreateHotkey("Ctrl-X", "Perform 'MoveTo'",          'UI_Lua import("/mods/cinematics/modules/ui-controller.lua").PerformMoveToMotion()'))
table.insert(HotkeysMoveTo, CreateHotkey("Ctrl-Tab", "Toggle 'MoveTo' Origin",  'UI_Lua import("/mods/cinematics/modules/ui-controller.lua").ToggleUseMoveToOrigin()'))

table.insert(HotkeysPan, CreateHotkey("Ctrl-E", "Toggle 'Pan' Origin",         'UI_Lua import("/mods/cinematics/modules/ui-controller.lua").TogglePanOrigin()'))
table.insert(HotkeysPan, CreateHotkey("Ctrl-R", "Set 'Pan' Origin",            'UI_Lua import("/mods/cinematics/modules/ui-controller.lua").SetPanOrigin()'))
table.insert(HotkeysPan, CreateHotkey("Ctrl-D", "Preview 'Pan' Origin",        'UI_Lua import("/mods/cinematics/modules/ui-controller.lua").PreviewPanOrigin()'))
table.insert(HotkeysPan, CreateHotkey("Ctrl-F", "Preview Pan",                 'UI_Lua import("/mods/cinematics/modules/ui-controller.lua").PreviewPan()'))
table.insert(HotkeysPan, CreateHotkey("Ctrl-C", "Perform Pan",                 'UI_Lua import("/mods/cinematics/modules/ui-controller.lua").PerformPan()'))

table.insert(HotkeysSpin, CreateHotkey("Ctrl-T", "Toggle 'Spin' Origin",       'UI_Lua import("/mods/cinematics/modules/ui-controller.lua").ToggleSpinOrigin()'))
table.insert(HotkeysSpin, CreateHotkey("Ctrl-Y", "Set 'Spin' Origin",          'UI_Lua import("/mods/cinematics/modules/ui-controller.lua").SetSpinOrigin()'))
table.insert(HotkeysSpin, CreateHotkey("Ctrl-G", "Preview 'Spin' Origin",       'UI_Lua import("/mods/cinematics/modules/ui-controller.lua").PreviewSpinOrigin()'))
table.insert(HotkeysSpin, CreateHotkey("Ctrl-H", "Preview Spin",               'UI_Lua import("/mods/cinematics/modules/ui-controller.lua").PreviewSpin()'))
table.insert(HotkeysSpin, CreateHotkey("Ctrl-B", "Perform Spin",               'UI_Lua import("/mods/cinematics/modules/ui-controller.lua").PerformSpin()'))

-- MOVE TO --

--- Stores all information related to the 'Move To' functionality
MoveTo = { }

UseMoveToOrigin = true

-- intialize
local camera = GetCamera("WorldCamera") 
MoveTo.Seconds = 2.0
MoveTo.Origin = camera:SaveSettings()
MoveTo.Target = camera:SaveSettings()
MoveTo.UseOrigin = true

-- SPIN --

local camera = GetCamera("WorldCamera") 

Spin = { }
Spin.HeadingRate = 0.1
Spin.ZoomRate = 0.4
Spin.Origin = camera:SaveSettings()
Spin.UseOrigin = true


-- PAN --

local camera = GetCamera("WorldCamera") 

Pan = { }
Pan.XRate = 1.0
Pan.ZRate = 0.0
Pan.Origin = camera:SaveSettings()
Pan.UseOrigin = true

--- Formats a float.
-- @param f The float to format
function FormatFloat(f)
    return string.format("%02g", f)
end

--- Formats a vector.
-- @param v The vector to format.
function FormatVector(v)
    local x = FormatFloat(v.x)
    local y = FormatFloat(v.y)
    local z = FormatFloat(v.z)

    return "(" .. x .. ", " .. y .. ", " .. z .. ")"
end

--- Formats a vector.
-- @param v The vector to format.
function FormatBoolean(v)
    if v then 
        return "true"
    else
        return "false"
    end
end