
--- Toggles the rendering of the UI.
function ToggleUI()
    local uiModel = import("/mods/cinematics/modules/ui-model.lua")
    uiModel.ConRenderUI = not uiModel.ConRenderUI

    ConExecute("ren_UI " .. tostring(uiModel.ConRenderUI))
end

--- Toggles whether the camera is free.
function ToggleFreeCam()
    ConExecute("cam_Free")
end

--- Toggles whether decals have a LOD setting.
function ToggleIgnoreDecalLOD()
    ConExecute('ren_IgnoreDecalLOD')
end

--- Toggles whether decals have a LOD setting.
function ToggleEaseInAndOut()
    local camera = GetCamera("WorldCamera")
    local uiModel = import("/mods/cinematics/modules/ui-model.lua")

    uiModel.easeInAndOut = not uiModel.easeInAndOut
    if uiModel.easeInAndOut then
        LOG("Enabling ease in and out")
        camera:EnableEaseInOut()
    else
        LOG("Disabling ease in and out")
        camera:DisableEaseInOut()
    end
end


--- Toggles whether decals have a LOD setting.
function ToggleReset()
    local camera = GetCamera("WorldCamera")
    camera:Reset()
    camera:RevertRotation()

    -- reset uiModel
    local uiModel = import("/mods/cinematics/modules/ui-model.lua")
    uiModel.nearFov = uiModel.oNearFov
    uiModel.farFov = uiModel.oFarFov
    uiModel.nearPitch = uiModel.oNearPitch
    uiModel.farPitch = uiModel.oFarPitch
    uiModel.shadowResolution = uiModel.oShadowResolution
    uiModel.shadowLOD = uiModel.oShadowLOD

    -- reset game
    ConExecute("cam_NearFov " .. uiModel.nearFov)
    ConExecute("cam_FarFov " .. uiModel.farFov)
    ConExecute("cam_NearPitch " .. uiModel.nearPitch)
    ConExecute("cam_FarPitch " .. uiModel.farPitch)
    ConExecute("ren_ShadowLOD " .. uiModel.shadowLOD)
    ConExecute("ren_ShadowSize " .. uiModel.shadowResolution)
end

--- Toggles whether select boxes are rendered.
function ToggleSelectBoxes()
    ConExecute("ren_SelectBoxes")
end
 
--- Stores the current camera configuration in 'MoveTo - Origin'
function SetMoveToOrigin()
    local uiModel = import("/mods/cinematics/modules/ui-model.lua")
    local camera = GetCamera("WorldCamera")
    uiModel.MoveTo.Origin = camera:SaveSettings()
    uiModel.MoveTo.UseOrigin = true
    LOG("Saved camera settings to 'MoveTo - Origin'")
end

--- Stores the current camera configuration in 'MoveTo - Target'
function SetMoveToTarget()
    local uiModel = import("/mods/cinematics/modules/ui-model.lua")
    local camera = GetCamera("WorldCamera")
    uiModel.MoveTo.Target = camera:SaveSettings()

    LOG("Saved camera settings to 'MoveTo - Target'")
end

--- Previews the camera configuration in 'MoveTo - Origin'
function PreviewMoveToOrigin()
    local uiModel = import("/mods/cinematics/modules/ui-model.lua")
    local camera = GetCamera("WorldCamera")
    camera:RestoreSettings(uiModel.MoveTo.Origin)
    camera:HoldRotation()

    LOG("Preview camera settings of 'MoveTo - Origin'")
end

--- Previews the camera configuration in 'MoveTo - Target'
function PreviewMoveToTarget()
    local uiModel = import("/mods/cinematics/modules/ui-model.lua")
    local camera = GetCamera("WorldCamera")
    camera:RestoreSettings(uiModel.MoveTo.Target)
    camera:HoldRotation()

    LOG("Preview camera settings of 'MoveTo - Target'")
end

--- Previews the camera configuration for 'MoveTo'
function PreviewMoveToMotion()
    local uiModel = import("/mods/cinematics/modules/ui-model.lua")
    local camera = GetCamera("WorldCamera")
    local original = camera:SaveSettings()

    -- retrieve information and perform motion
    local origin = uiModel.MoveTo.Origin 
    local target = uiModel.MoveTo.Target
    local seconds = uiModel.MoveTo.Seconds

    if uiModel.MoveTo.UseOrigin then 
        camera:RestoreSettings(origin)
        camera:HoldRotation()
    end

    camera:MoveTo(target.Focus, {target.Heading, target.Pitch, 0}, target.Zoom, seconds)

    -- reset
    ForkThread(
        function()
            WaitSeconds(seconds + 1)
            camera:RestoreSettings(original)
            camera:HoldRotation()
        end
    )

    LOG("Preview camera settings of 'MoveTo'")
end

--- Performs the 'MoveTo' motion.
function PerformMoveToMotion()
    local uiModel = import("/mods/cinematics/modules/ui-model.lua")
    ForkThread(
        function () 
            local camera = GetCamera("WorldCamera")

            -- retrieve information and perform motion
            local origin = uiModel.MoveTo.Origin 
            local target = uiModel.MoveTo.Target
            local seconds = uiModel.MoveTo.Seconds

            if uiModel.MoveTo.UseOrigin then 
                camera:RestoreSettings(origin)
                camera:HoldRotation()
            end

            camera:MoveTo(target.Focus, {target.Heading, target.Pitch, 0}, target.Zoom, seconds)

            LOG("Perform camera settings of 'MoveTo'")
        end
    )
end

--- Adds to the transition time.
function AddTransitionTime()
    local uiModel = import("/mods/cinematics/modules/ui-model.lua")
    uiModel.MoveTo.Seconds = uiModel.MoveTo.Seconds + 1
end

--- Deducts from the transition time.
function DeductTransitionTime()
    local uiModel = import("/mods/cinematics/modules/ui-model.lua")
    uiModel.MoveTo.Seconds = uiModel.MoveTo.Seconds - 1
end

--- Toggles whether we use the current camera or the set origin for the MoveTo animation.
function ToggleUseMoveToOrigin()
    local uiModel = import("/mods/cinematics/modules/ui-model.lua")
    uiModel.MoveTo.UseOrigin = not uiModel.MoveTo.UseOrigin
end

--- Toggles whether we should use the pan origin or not. Allows us to start panning while we're in another camera animation.
function TogglePanOrigin()
    local uiModel = import("/mods/cinematics/modules/ui-model.lua")
    uiModel.Pan.UseOrigin = not uiModel.Pan.UseOrigin
end

--- Defines the origin of the panning.
function SetPanOrigin()
    local uiModel = import("/mods/cinematics/modules/ui-model.lua")
    local camera = GetCamera("WorldCamera")
    uiModel.Pan.Origin = camera:SaveSettings()
    uiModel.Pan.UseOrigin = true
end

--- Previes the pan origin
function PreviewPanOrigin()
    local uiModel = import("/mods/cinematics/modules/ui-model.lua")
    local camera = GetCamera("WorldCamera")
    camera:RestoreSettings(uiModel.Pan.Origin)
end

--- Toggles whether we use the current camera or the set origin for the MoveTo animation.
function PreviewPan()
    local uiModel = import("/mods/cinematics/modules/ui-model.lua")
    local camera = GetCamera("WorldCamera")

    -- use current camera settings, unless we use the origin defined by ourselves
    local source = camera:SaveSettings()
    if uiModel.Pan.UseOrigin then 
        source = uiModel.Pan.Origin 
    end

    -- perform the panning
    local multiplier = 10
    local seconds = uiModel.Pan.Seconds
    local target = { source.Focus.x + uiModel.Pan.XRate * multiplier, source.Focus.y, source.Focus.z + uiModel.Pan.ZRate * multiplier }
    camera:MoveTo(target, {source.Heading, source.Pitch, 0}, source.Zoom, multiplier)

    -- reset
    ForkThread(
        function()
            WaitSeconds(seconds + 1)
            camera:RestoreSettings(source)
            camera:HoldRotation()
        end
    )
end

--- Toggles whether we use the current camera or the set origin for the MoveTo animation.
function PerformPan()
    local uiModel = import("/mods/cinematics/modules/ui-model.lua")
    local camera = GetCamera("WorldCamera")

    -- use current camera settings, unless we use the origin defined by ourselves
    local source = camera:SaveSettings()
    if uiModel.Pan.UseOrigin then 
        source = uiModel.Pan.Origin 
    end

    -- perform the panning
    local multiplier = 10
    local seconds = uiModel.Pan.Seconds
    local target = { source.Focus.x + uiModel.Pan.XRate * multiplier, source.Focus.y, source.Focus.z + uiModel.Pan.ZRate * multiplier }
    camera:MoveTo(target, {source.Heading, source.Pitch, 0}, source.Zoom, multiplier)
end

--- Toggles whether we should use the pan origin or not. Allows us to start panning while we're in another camera animation.
function ToggleSpinOrigin()
    local uiModel = import("/mods/cinematics/modules/ui-model.lua")
    uiModel.Spin.UseOrigin = not uiModel.Spin.UseOrigin
end

--- Defines the origin of the Spinning.
function SetSpinOrigin()
    local uiModel = import("/mods/cinematics/modules/ui-model.lua")
    local camera = GetCamera("WorldCamera")
    uiModel.Spin.Origin = camera:SaveSettings()
    uiModel.Spin.UseOrigin = true
end

--- Previes the Spin origin
function PreviewSpinOrigin()
    local uiModel = import("/mods/cinematics/modules/ui-model.lua")
    local camera = GetCamera("WorldCamera")
    camera:RestoreSettings(uiModel.Spin.Origin)
end

--- Toggles whether we use the current camera or the set origin for the MoveTo animation.
function PreviewSpin()
    local uiModel = import("/mods/cinematics/modules/ui-model.lua")
    local camera = GetCamera("WorldCamera")

    -- use current camera settings, unless we use the origin defined by ourselves
    local source = camera:SaveSettings()
    if uiModel.Spin.UseOrigin then 
        source = uiModel.Spin.Origin 
    end

    -- perform the Spinning
    local seconds = 5  -- has no limit in practice
    camera:RestoreSettings(source)
    camera:HoldRotation()
    camera:Spin(uiModel.Spin.HeadingRate, uiModel.Spin.ZoomRate)

    -- reset
    ForkThread(
        function()
            WaitSeconds(seconds + 1)
            camera:RestoreSettings(source)
            camera:HoldRotation()
        end
    )
end

--- Toggles whether we use the current camera or the set origin for the MoveTo animation.
function PerformSpin()
    local uiModel = import("/mods/cinematics/modules/ui-model.lua")
    local camera = GetCamera("WorldCamera")

    -- use current camera settings, unless we use the origin defined by ourselves
    local source = camera:SaveSettings()
    if uiModel.Spin.UseOrigin then 
        source = uiModel.Spin.Origin 
    end

    -- perform the Spinning
    camera:RestoreSettings(source)
    camera:HoldRotation()
    camera:Spin(uiModel.Spin.HeadingRate, uiModel.Spin.ZoomRate)
end